#### @universis/converter

Universis API extension for converting presentation documents and reports to pdf format.

@universis/converter is a part of [@universis/templates](https://gitlab.com/universis/universis-templates) project
 which is the primary document template engine of [@universis/api](https://gitlab.com/universis/universis-api).

#### Install

    npm i @universis/converter@next
    
Install [LibreOffice](https://www.libreoffice.org/) or use docker engine to install it as a docker container. 
LibreOffice - a powerful open source office suite- and its tools are used by DocumentConverterService in order to convert docx to pdf documents.


#### Usage

Register DocumentConverterService in section services of Universis api server configuration:

    # server/config/app.json
        
    {
        "services": [
            ...
            { "serviceType": "@universis/converter#DocumentConverterService" }
        ]
    ...
    }
 
and use it in order to convert any *.docx file to pdf:

    const docFile = path.resolve(__dirname, '.tmp/HelloWorld.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(docFile, buffer, (err)=> {
                if (err) {
                    return reject(err);
                }
                converterService.convertFile(docFile, docFile.replace(/.docx$/ig, '.pdf')).then(()=> {
                    return resolve();
                }).catch( err => {
                    return reject(err);
                });
    
            });
        });
        
##### Configure document converter

DocumentConverterService tries to find LibreOffice installation and uses ```libreoffice``` 
executable in order to convert docx files. If ```libreoffice``` is not available  via PATH environment variable
 use application configuration to set the full path of ```libreoffice```.

e.g. for Mac OS

    "settings": {
        ...
        "universis": {
            "converter": {
                "command": "/Applications/LibreOffice.app/Contents/MacOS/soffice"
            },
        ...
        }
        
#### DocumentConverterService

DocumentConverterService class converts office documents to pdf.

##### DocumentConverterService#convertFile(inFile: string,outFile: string): Promise&lt;any&gt;

Converts the given file based on the file extension specified on the output file.

    const inFile = path.resolve(__dirname, '.tmp/HelloWorld.docx');
    const outFile = path.resolve(__dirname, '.tmp/HelloWorld.pdf');
    // convert inpu file
    await converterService.convertFile(docFile, outFile);
    
