import app from './standalone/app';
import {DocumentConverterService} from './service'

module.exports.app = app;
module.exports.DocumentConverterService = DocumentConverterService;
