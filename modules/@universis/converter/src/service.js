import {Args} from '@themost/common';
import {IApplication, ApplicationService} from '@themost/common/app';
import path from 'path';
import {spawn} from 'child_process';
import {rename} from 'fs';
class DocumentConverterService extends ApplicationService {
    /**
     *
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        // get options
        const options = Object.assign({
            "command": "libreoffice"
        }, app.getConfiguration().getSourceAt('settings/universis/converter'));
        Object.defineProperty(this, 'options', {
            value: options
        });
    }

    // noinspection JSUnusedGlobalSymbols
    // noinspection JSMethodCanBeStatic
    /**
     * Converts a file and saves it to the output path specified
     * @param {string} inFile
     * @param {string} outFile
     */
    async convertFile(inFile, outFile) {
        Args.notString(inFile, 'Input file path should be a valid string');
        Args.notString(outFile, 'Output file path should be a valid string');
        // get file format
        const format = path.extname(outFile).substr(1);
        const outDir = path.dirname(outFile);
        await new Promise((resolve, reject) => {
            // `libreoffice --headless --convert-to ${format} "${inFile}" --outdir "${outDir}"`;
            const childProcess = spawn(this.options.command, [
                '--nologo',
                '--invisible',
                '--headless',
                '--convert-to',
                format,
                inFile,
                '--outdir',
                outDir
            ]);
            childProcess.stderr.on('data', (data) => {
                if (data instanceof Buffer) {
                    return reject(data.toString());
                }
                console.log('ERROR', data);
            });
            childProcess.on('close', (code) => {
                if (code !== 0) {
                    return reject(`An error occurred while trying to convert input file. Process exited with code ${code}`);
                }
                // rename file
                const outConvertFile = path.resolve(outDir, path.basename(outFile));
                rename(outConvertFile, outFile, (err)=> {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
            });
        });
    }

}

module.exports.DocumentConverterService = DocumentConverterService;
