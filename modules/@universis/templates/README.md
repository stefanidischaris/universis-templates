#### universis-templates

Universis Api Server extension for managing report templates. @universis/templates uses 
[@themost/web](https://github.com/themost-framework/themost) Angular JS for server applications template to render
documents and reports.

![Editor](../../../docs/google-drive-edit-document.png)

The following code snippet uses a simple template to render a document based on the data specified.

        // get template service
        const templateService = context.getApplication().getService(DocumentTemplateService);
        // get template path
        const templatePath = path.resolve(__dirname, 'files/HelloTemplate.docx');
        // render template
        const buffer = await templateService.render(context, templatePath, {
            department: {
                name: 'Computer Science',
                organization: {
                    name: 'Educational Institute'
                }
            },
            message: 'Hello World'
        });
        // save file
        const outFile = path.resolve(__dirname, '.tmp/HelloTemplate.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(outFile, buffer, (err)=> {
               if (err) {
                   return reject(err);
               }
               return resolve();
            });
        });

The result will be a document like this:

![Render document](../../../docs/render-document.png)

##### Export document

Use @universis/converter#DocumentConverterService to convert a document to pdf.

    // get template service
    const templateService = context.getApplication().getService(DocumentTemplateService);
    /**
     * get DocumentConverterService service
     * @type {DocumentConverterService}
     */
    const converterService = context.getApplication().getService(DocumentConverterService);
    // get template path
    const templatePath = path.resolve(__dirname, 'files/HelloTemplate.docx');
    // render template
    const buffer = await templateService.render(context, templatePath, {
        department: {
            name: 'Computer Science',
            organization: {
                name: 'Educational Institute'
            }
        },
        message: 'Hello World'
    });
    // save file
    const docFile = path.resolve(__dirname, '.tmp/HelloTemplate.docx');
    await new Promise((resolve, reject) => {
        fs.writeFile(docFile, buffer, (err)=> {
            if (err) {
                return reject(err);
            }
            converterService.convertFile(docFile, docFile.replace(/.docx$/ig, '.pdf')).then(()=> {
                return resolve();
            }).catch( err => {
                return reject(err);
            });

        });
    });

and the result will be a pdf document:

![Export document](../../../docs/export-document.png)

##### Usage

Install @universis/templates and @universis/converter modules

    npm i @universis/templates @universis/converter

Install [LibreOffice](https://www.libreoffice.org/) or use docker engine to install it as docker container. 
LibreOffice - a powerful open source office suite- and its tools are used by DocumentConverterService in order to convert docx to pdf documents.

add DocumentTemplateService to [@universis/api](https://gitlab.com/universis/universis-api) 
services section of application configuration:

    # server/config/app.json
    
    {
        "services": [
            ...
            { "serviceType": "@universis/templates#DocumentTemplateService" }
        ]
    ...
    }
    
add DocumentTemplateLoader to [@universis/api](https://gitlab.com/universis/universis-api) 
services section of application configuration:

    # server/config/app.json
    
    {
        "services": [
            ...
            { "serviceType": "@universis/templates#DocumentTemplateLoader" }
        ]
    ...
    }

##### Use Google Drive REST API

If your intended to use GoogleDriveTemplateLoader to load templates using [Google Drive REST API](https://developers.google.com/drive/api/v3/about-sdk), 
register GoogleDriveTemplateLoader:

    # server/config/app.json
        
        {
            "services": [
                ...
                { 
                    "stategyType": "@universis/templates#DocumentTemplateLoader",
                    "serviceType": "@universis/templates#GoogleDriveTemplateLoader"
                }
            ]
        ...
        }
        
Follow [OAuth 2.0 for Server to Server Applications instructions](https://developers.google.com/identity/protocols/OAuth2ServiceAccount) 
for generating a service account for google drive api and set account credentials to application configuration:

    "settings": {
    ...
        "universis": {
                "converter": {
                    "command": "libreoffice"
                },
                "templates": {
                    "drive": {
                        "credentials": {
                            "type": "service_account",
                            "project_id": "project",
                            "private_key_id": "...",
                            "private_key": "...",
                            "client_email": "...",
                            "client_id": "...",
                            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                            "token_uri": "https://oauth2.googleapis.com/token",
                            "auth_provider_x509_cert_url": "...",
                            "client_x509_cert_url": "..."
                        }
                    }
                }
            }
        }
     ...

and render documents downloaded from google drive:

    // get template service
    const templateService = context.getApplication().getService(DocumentTemplateService);
    /**
     * get DocumentConverterService service
     * @type {DocumentConverterService}
     */
    const converterService = context.getApplication().getService(DocumentConverterService);
    // get google drive document by name
    const templatePath = 'HelloTemplate.docx';
    // render template
    const buffer = await templateService.render(context, templatePath, {
        department: {
            name: 'Computer Science',
            organization: {
                name: 'Educational Institute'
            }
        },
        message: 'Hello World'
    });
    // save file
    const docFile = path.resolve(__dirname, '.tmp/HelloTemplate.docx');
    await new Promise((resolve, reject) => {
        fs.writeFile(docFile, buffer, (err)=> {
            if (err) {
                return reject(err);
            }
            converterService.convertFile(docFile, docFile.replace(/.docx$/ig, '.pdf')).then(()=> {
                return resolve();
            }).catch( err => {
                return reject(err);
            });

        });
    });

##### Use document converter

Finally add DocumentConverterService to [@universis/api](https://gitlab.com/universis/universis-api) 
services section of application configuration:

    # server/config/app.json
    
    {
        "services": [
            ...
            { "serviceType": "@universis/converter#DocumentConverterService" }
        ]
    ...
    }

and start using document templates for creating Universis reports.

    
