class DocumentTemplateNotFound extends Error {
    constructor() {
        super();
        this.message = 'Document template not found';
        this.code = 'ERR_TEMPLATE_NOT_FOUND';
        if (typeof Error.captureStackTrace === 'function') {
            // noinspection JSUnresolvedFunction
            Error.captureStackTrace(this, this.constructor);
        }
    }
}

module.exports.DocumentTemplateNotFound = DocumentTemplateNotFound;
