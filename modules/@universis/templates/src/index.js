import {DocumentTemplateService, DocumentTemplateLoader, GoogleDriveTemplateLoader} from './service';
import {DocumentTemplateNotFound} from './errors';
module.exports.DocumentTemplateLoader = DocumentTemplateLoader;
module.exports.GoogleDriveTemplateLoader = GoogleDriveTemplateLoader;
module.exports.DocumentTemplateService = DocumentTemplateService;

module.exports.DocumentTemplateNotFound = DocumentTemplateNotFound;

