import {IApplication, ApplicationService} from '@themost/common/app';
import {NgEngine} from '@themost/web';
import docx4js from 'docx4js';
import tmp from 'tmp';
import fs from 'fs';
import path from 'path';
import {google} from 'googleapis';
import {DocumentTemplateNotFound} from './errors';

class DocumentTemplateLoader extends ApplicationService {
    constructor(app) {
          super(app);
    }

    // noinspection JSMethodCanBeStatic
    async fetch(template) {
        return template;
    }

}

const GoogleDriveExportFormats = {
  "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
};

class GoogleDriveTemplateLoader extends DocumentTemplateLoader  {
    constructor(app) {
        super(app);
        // get options
        const options = Object.assign({ }, app.getConfiguration().getSourceAt('settings/universis/templates'));
        Object.defineProperty(this, 'options', {
            value: options
        });
    }

    authorize() {
        return new Promise((resolve, reject) => {
            if (this.jwtClient) {
                // validate token expiration
                let expires = false;
                if (this.jwtClient.gtoken && this.jwtClient.gtoken.expiresAt) {
                    expires = new Date() > new Date(this.jwtClient.gtoken.expiresAt);
                }
                if (!expires) {
                    return resolve(this.jwtClient);
                }
            }
            const credentials = this.options && this.options.drive && this.options.drive.credentials;
            if (credentials == null) {
                return reject(new Error('Drive configuration is missing or is inaccessible'))
            }
            // configure a JWT auth client
            let jwtClient = new google.auth.JWT(
                credentials.client_email,
                null,
                credentials.private_key,
                ['https://www.googleapis.com/auth/drive']);
            //authenticate request
            const self = this;
            jwtClient.authorize((err, token) => {
                if (err) {
                    return  reject(err);
                }
                // set client
                self.jwtClient = jwtClient;
                // return client
                return resolve(self.jwtClient);
            });
        });
    }


    // noinspection JSMethodCanBeStatic
    async fetch(template) {
        const jwtClient  = await this.authorize();
        let drive = google.drive('v3');
        // get file list
        return await new Promise((resolve, reject) => {
            drive.files.list({
                auth: jwtClient
            },function (err, response) {
                if (err) {
                    return reject(err);
                }
                // get google drive files
                const files = response.data.files;
                // get file by name
                let file = files.find( file => {
                    return file.name === template.replace(/.docx$/ig, '');
                });
                // throw error if the specified file cannot be found
                if (file == null) {
                    return reject(Object.assign(new DocumentTemplateNotFound()));
                }
                // get temp file name
                tmp.file({
                    postfix: '.docx'
                }, (err, tempPath, fd, cleanupCallback) => {
                    if (err) {
                        cleanupCallback();
                        return reject(err);
                    }
                    // create a write stream for template
                    const dest = fs.createWriteStream(tempPath);
                    // call google drive api export function
                    drive.files.export({
                        auth: jwtClient,
                        fileId: file.id,
                        mimeType: GoogleDriveExportFormats.docx
                    }, {
                        // important specify response as arraybuffer
                        responseType: 'arraybuffer'
                    }, (err, response) => {
                        fs.writeFile(tempPath, response.data, err => {
                            if (err) {
                                cleanupCallback();
                                return reject(err);
                            }
                            return resolve(tempPath);
                        });
                    });
                });
            });
        });
    }
}

class DocumentTemplateService extends ApplicationService  {
    /**
     *
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.bootstrap( angular => {
            return angular.module('template', []).config(function($interpolateProvider) {
                if ($interpolateProvider) {
                    $interpolateProvider.startSymbol('{{');
                    $interpolateProvider.endSymbol('}}');
                }
            });
        });
        // register document template loader
        if (!app.hasStrategy(DocumentTemplateLoader)) {
            app.useStrategy(DocumentTemplateLoader, DocumentTemplateLoader);
        }
    }

    /**
     * @param {Function} bootstrapFunc
     * @returns {DocumentTemplateService}
     */
    bootstrap(bootstrapFunc) {
        this.hasBootstrap = bootstrapFunc;
        return this;
    }

    // noinspection JSUnusedGlobalSymbols
    // noinspection JSMethodCanBeStatic
    /**
     * Renders the given document template
     * @param {DataContext} context
     * @param {string} template - A string which represents a template file path
     * @param {*} data - Any data that is going to be used while rendering template
     */
    async render(context, template, data) {
        // try to load template
        const blob = await this.getApplication().getStrategy(DocumentTemplateLoader).fetch(template);
        const docx = await docx4js.load(blob);
        // get content
        let content = docx.officeDocument.content.xml();
        // replace special characters
        content = content.replace(/``/g,'\'')
            .replace(/&amp;/g,'&')
            .replace(/&lt;/g,'<')
            .replace(/&gt;/g,'<');
        // initialize engine
        const engine = new NgEngine(context);
        // set bootstrap function
        if (typeof this.hasBootstrap === 'function') {
            engine.bootstrap(this.hasBootstrap);
        }
        const value = await engine.renderString(`
            <html>
                <head>
                </head>
                <body>
                <script id="officeDocument" type="text/xmldata">
                    ${content}
                </script>
                </body>
            </html>
            `, {
            model: data || {}
        });
        // get w:document tag
        const matches = /<w:document(.*?)>(.*?)<\/w:document>/ig.exec(value);
        let str;
        if (matches) {
            str = matches[2];
        }
        // set document inner xml
        docx.officeDocument.content('w\\:document').html(str);
        // save, open file ands return a buffer
        return await new Promise((resolve, reject) => {
            // get temp file name
            tmp.file({
                postfix: '.docx'
            }, (err, path, fd, cleanupCallback) => {
                if (err) {
                    return reject(err);
                }
                // save temporary file
                docx.save(path).then(()=> {
                    // read temporary file
                    fs.readFile(path, function (err, buffer) {
                        // call cleanup callback
                        cleanupCallback();
                        // check error
                        if (err) {
                            return reject(err);
                        }
                        // return file as buffer
                        return resolve(buffer);
                    });
                }).catch( err => {
                    cleanupCallback();
                    return reject(err);
                });
            });
        });
    }
}

module.exports.DocumentTemplateLoader = DocumentTemplateLoader;
module.exports.GoogleDriveTemplateLoader = GoogleDriveTemplateLoader;
module.exports.DocumentTemplateService = DocumentTemplateService;
