import express from "express";
import {ExpressDataApplication} from '@themost/express';
import path from 'path';
// initialize app
const app = express();
// initialize data application
const dataApplication = new ExpressDataApplication(path.resolve(__dirname, 'config'));
// set express data application
app.set('ExpressDataApplication', dataApplication);
// export app
module.exports = app;

