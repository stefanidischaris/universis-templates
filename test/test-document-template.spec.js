import app from './app';
import {ExpressDataApplication} from '@themost/express';
import {DocumentTemplateService, DocumentTemplateLoader, GoogleDriveTemplateLoader} from '../modules/@universis/templates/src/service';
import {assert} from 'chai';
import path from 'path';
import fs from 'fs';
import GraduatedStudent from './data/GraduatedStudent';
import {DocumentConverterService} from '../modules/@universis/converter/src/service';

describe('Test DocumentTemplateService', ()=> {

    it('should create instance', ()=> {
        // get data application
        const dataApplication = app.get(ExpressDataApplication.name);
        const service = new DocumentTemplateService(dataApplication);
        assert.isObject(service);
    });
    it('should register service', ()=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        dataApplication.useService(DocumentTemplateService);
        assert.isObject(dataApplication.getService(DocumentTemplateService));
        assert.instanceOf(dataApplication.getService(DocumentTemplateService), DocumentTemplateService);
    });

    it('should render document', async ()=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // use service
        dataApplication.useService(DocumentTemplateService);
        /**
         * @type {DocumentTemplateService}
         */
        const templateService = dataApplication.getService(DocumentTemplateService);
        assert.instanceOf(templateService, DocumentTemplateService);
        // render template
        const context = dataApplication.createContext();
        assert.isObject(context);
        const buffer = await templateService.render(context, path.resolve(__dirname, 'files/simple-template.docx'), {
            institute: 'Education Institute Alpha',
            department: 'Computer Science'
        });
        assert.instanceOf(buffer, Buffer);
        // save file
        const outFile = path.resolve(__dirname, '.tmp/simple-template.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(outFile, buffer, (err)=> {
               if (err) {
                   return reject(err);
               }
               return resolve();
            });
        });
        assert.isOk(fs.existsSync(outFile), 'Output file cannot be found');
    });

    it('should render student document', async ()=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // use service
        dataApplication.useService(DocumentTemplateService);
        dataApplication.useService(DocumentConverterService);
        /**
         * @type {DocumentTemplateService}
         */
        const templateService = dataApplication.getService(DocumentTemplateService);
        dataApplication.useStrategy(DocumentTemplateLoader, GoogleDriveTemplateLoader);
        assert.instanceOf(templateService, DocumentTemplateService);
        // render template
        const context = dataApplication.createContext();
        assert.isObject(context);
        const buffer = await templateService.render(context, 'GraduationCertificate.docx', GraduatedStudent);
        assert.instanceOf(buffer, Buffer);
        // save file
        const outFile = path.resolve(__dirname, '.tmp/GraduationCertificateExport.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(outFile, buffer, (err)=> {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
        assert.isOk(fs.existsSync(outFile), 'Output file cannot be found');
        // finally convert document to pdf
        /**
         * @type {DocumentConverterService}
         */
        const converter = dataApplication.getService(DocumentConverterService);
        await converter.convertFile(outFile, outFile.replace(/.docx/ig, '.pdf'));
    });

});
